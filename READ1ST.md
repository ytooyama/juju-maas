# インストールからデプロイまでの流れ

## 1.MAASのセットアップ

* https://maas.io/install


## 2.Jujuのインストール

* https://jaas.ai/docs/installing

## 3.Juju Cloudの設定

* https://jaas.ai/docs/clouds

### MAAS Cloudの場合

* https://jaas.ai/docs/maas-cloud

## 4.デプロイ

各ディレクトリーのREADMEを見てください。


# システム構成例
以下は構成例です。物理マシン、仮想マシン、コンテナーを組み合わせて構築できます。

## ネットワーク
* 2つのUnTagなVLANを用意
* VLAN1: 管理用ネットワーク用。MAAS DHCP/DNS用
* VLAN2: OpenStack Externalネットワーク用。DHCP/DNSが稼働していないネットワークを用意

## Linux KVMサーバー
* MAASサーバーとJujuクライアント用
* 32GB程度かそれ以上のメモリー
* システム用とVMイメージ用のストレージを分ける(推奨)

## MAAS/Jujuコントロールサーバー
* MAASサーバーとJujuクライアント用仮想マシン
* インストールするMAASに合わせたUbuntuバージョンをインストール
* VLAN1: 管理用ネットワークに接続
* 最低2CPU,8GBメモリー,30GBストレージ以上のスペックを用意

## 物理ノード
Jujuで利用する物理サーバーはMAAS管理下にする。

* Linux KVMを実行する環境とは別に最小、3つの物理サーバーを用意
* ストレージはハードディスクよりもSSD(Solid State Drive)を推奨
* VLAN1,VLAN2に接続
* VLAN1側で、MAASダッシュボードで利用するサブネットを設定
* VLAN1側で、DHCPサーバーからIPアドレスを割り当てるように設定
* MAASではVLAN2側は特に設定しないで良い
