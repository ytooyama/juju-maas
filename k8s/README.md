# Kubernetes deployment Sample YAML

## デプロイ前に確認すると良い情報
KubernetesをJujuでデプロイする場合はここを見たほうが良いのかもしれません。

### ドキュメント
* https://ubuntu.com/kubernetes/docs

### Stable Bundle

#### プロダクション
* [CDK](https://jaas.ai/canonical-kubernetes)
* [K8s + Canal](https://jaas.ai/canonical-kubernetes-canal)
* [K8s + Calico](https://jaas.ai/u/containers/kubernetes-calico)

#### テスト用
* [K8s Core](https://jaas.ai/kubernetes-core)
* [K8s CoreのDocker Runtime利用の最終バージョン](https://jaas.ai/kubernetes-core/732)



## このリポジトリーにあるファイルについて

### 実行の前に
[MAAS cloud](https://jaas.ai/docs/maas-cloud)を使うのを想定しています。2つのサーバーにYAMLで指定する[MAASタグ](https://maas.io/docs/tags)を設定したノードを用意しましょう。

### k8score-876.yaml
* 2 Node Minimal Kubernetes
* [Kubernetes Core #876](https://jaas.ai/kubernetes-core/876)


## デプロイ方法

```
% juju add-model k8s
% juju switch k8s
% juju deploy hoge.yaml
```

## 確認方法

```
% juju status
% juju debug-log
```

or `juju gui`