# OpenStack deployment Sample YAML

## デプロイ前に確認すると良い情報
OpenStackをJujuでデプロイする場合はここを見たほうが良いのかもしれません。

### Charms Release Note
* https://docs.openstack.org/charm-guide/latest/release-notes.html

### Stable Bundle
* https://github.com/openstack-charmers/openstack-bundles
* https://jaas.ai/openstack-base/bundle

## Bundleの最近のアップデート
* [Openstack Base #60](https://jaas.ai/openstack-base/bundle/60) Rockyが標準
* [Openstack Base #61](https://jaas.ai/openstack-base/bundle/61) Steinが標準
* [Openstack Base #62](https://jaas.ai/openstack-base/bundle/62) Placementが追加、Train以降サポート

## このリポジトリーにあるファイルについて

### 実行の前に
[MAAS cloud](https://jaas.ai/docs/maas-cloud)を使うのを想定しています。2つのサーバーにYAMLで指定する[MAASタグ](https://maas.io/docs/tags)を設定したノードを用意しましょう。

### openstack-base-61.yaml
* 2 Node Minimal OpenStack
* [Openstack Base #62](https://jaas.ai/openstack-base/bundle/62)

### openstack-base-62.yaml
* 2 Node Minimal OpenStack
* [Openstack Base #61](https://jaas.ai/openstack-base/bundle/61)

## デプロイ方法

```
% juju add-model openstack
% juju switch openstack
% juju deploy hoge.yaml
```

## 確認方法

```
% juju status
% juju debug-log
```

or `juju gui`